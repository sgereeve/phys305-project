#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')
from scipy.io import loadmat
import matplotlib.pyplot as plt
import numpy as np, pandas as pd
from scipy.optimize import minimize
import scipy.stats as stats
from scipy.special import factorial
from scipy.optimize import curve_fit
from scipy.stats import norm
pi=np.pi


# In[2]:


# Importing and plotting data with errorbars

W=loadmat('hist_data2.mat',mat_dtype=True,squeeze_me=True)
locals().update({k : W[k] for k in ['e','n']})
plt.plot(e,n,label='Data')
plt.errorbar(e,n,(n**0.5),fmt='b*') 


# In[3]:


# Guessing initial parameters, defining Gaussian function, plotting against data

a= 4000
mu=125000
s=200

def Gauss(e,a,mu,s):
    '''Defining the Gaussian function'''
    return  a*np.exp(-(((e-mu)**2)/(2*(s**2))))

plt.plot(e,Gauss(e,a,mu,s), label='1st fit attempt')
plt.errorbar(e,n,(n**0.5),fmt='g*')
plt.title('Gaussian fit')
plt.xlabel('Energy (MeV)')
plt.ylabel('Number of events')
plt.legend()
plt.show()


# In[4]:


# Optimising parameters

popt, pcov = curve_fit(Gauss, e, n, p0 = [a,mu,s])
popt


# In[5]:


# Using new parameters, plotting against data

a=3184.28139506
mu=124993.22642512
s=188.43306816


def Gauss(e,a,mu,s):
    '''Defining the Gaussian function'''
    return  a*np.exp(-(((e-mu)**2)/(2*(s**2))))

plt.errorbar(e,n,(n**0.5),fmt='g*')
plt.plot(e,Gauss(e,a,mu,s), label='data fit')
plt.title('Fitted function for Higgs data')
plt.xlabel('Energy (MeV)')
plt.ylabel('Number of events')
plt.legend()
plt.show()


# In[6]:


# Residuals plot to show goodness of fit

res=n-Gauss(e,a,mu,s)
d_res=n**0.5

plt.ylabel('Residuals- Number of events')
plt.xlabel('Energy (MeV)')
plt.title('residuals plot for the Higgs function')
plt.errorbar(e,res,d_res,fmt='r*')
plt.show()


# In[7]:


# Calculating reduced Chi-squared and test statistic, to quantise goodness of fit

from scipy.stats import chi2

chisq=sum((res**2)/n)
chisq_ndf=chisq/(len(n)-2)
chisq_array=np.linspace(0,80,10000)
chisq_pdf=chi2.pdf(chisq_array,(len(n)-2))
chi_test=1-chi2.cdf(chisq,len(n)-2)
plt.plot(chisq_array, chisq_pdf, label='Chi-Squared distribution')
plt.plot(chisq,0.046,'r*', label='Chi-Squared value')
plt.legend()
plt.xlabel('Chi-Squared array')
plt.ylabel('PDF')
plt.title('Chi-Squared distribution of Higgs data and model Higgs function')

print('The standard deviation of the array is {0:1.3f}'.format(np.std(chisq_array)))
print('The test statistic is {0:1.4f}'.format(chi_test))
print('The reduced Chi-Squared value is {0:1.3f}'.format(chisq_ndf))
print('The maximum of the peak is {0:1.3f}'.format(max(chisq_pdf)))


# In[8]:


sigma=((1/len(n))*sum((e-mu)**2))**0.5
print(sigma) 

sqrt_N= (sum(a*np.exp(-((e-mu)**2)/(2*sigma**2))))**0.5
print(sqrt_N)

dmu=sigma/sqrt_N
print(dmu)


# In[9]:


print('Mass of the Higgs = {0:1.1f}'.format(mu), '+/- {0:1.1f}'.format(dmu),'MeV')


# In[ ]:




