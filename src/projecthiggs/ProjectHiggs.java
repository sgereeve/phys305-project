package projecthiggs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class ProjectHiggs {
    
    static Random randGen = new Random();

    public static void main(String[] args) throws FileNotFoundException {
        
        //Define mass of muon
        double mass = 0.106; //MeV 
        
        //Define number of events
        //entire event file
        //int lines = 21835;
        //ATLAS
        //int lines = 20;
        //CMS
        int lines = 24;
        
        //Define parameters r0, r1 and r2
        //ATLAS
        //double r0 = 0;
        //double r1 = 0.025;
        //double r2 = 0.0001;
        //CMS
        double r0 = 0;
        double r1 = 0.01;
        double r2 = 0.00006;        

        //Get scanner instance
        Scanner scanner = new Scanner(new File("SM_H4l.csv"));
         
        //Set the delimiter used in file
        scanner.useDelimiter(",|\n");
         
        //define histogram for m(4mu) 
        //for all events
        //Histogram mass4mu = new Histogram(500,100000.0,150000.0,"m(4mu) histogram");
        //for 50 events
        Histogram mass4mu = new Histogram(100,110.0,130.0,"m(4mu) histogram");
        
        //define histogram for m(4mu) measured
        //for all events
        //Histogram mass4muMeasured = new Histogram(500,100000.0,150000.0,"m(4mu) measured histogram");
        //for 50 events
        Histogram mass4muMeasured = new Histogram(100,50.0,300.0,"m(4mu) measured histogram");
        
        //Get all tokens and store them as particle components
        for (int n=0;n<lines;n++) {
        
            Particle muon1 = new Particle();
            muon1.px = (Double.parseDouble(scanner.next()))/1000;
            muon1.py = (Double.parseDouble(scanner.next()))/1000;
            muon1.pz = (Double.parseDouble(scanner.next()))/1000;
            muon1.Q = Double.parseDouble(scanner.next());
            double muon1_energy = muon1.Energy(mass);
            
            Particle muon2 = new Particle();
            muon2.px = (Double.parseDouble(scanner.next()))/1000;
            muon2.py = (Double.parseDouble(scanner.next()))/1000;
            muon2.pz = (Double.parseDouble(scanner.next()))/1000;
            muon2.Q = Double.parseDouble(scanner.next());
            double muon2_energy = muon2.Energy(mass);
            
            Particle muon3 = new Particle();
            muon3.px = (Double.parseDouble(scanner.next()))/1000;
            muon3.py = (Double.parseDouble(scanner.next()))/1000;
            muon3.pz = (Double.parseDouble(scanner.next()))/1000;
            muon3.Q = (Double.parseDouble(scanner.next()))/1000;
            double muon3_energy = muon3.Energy(mass);
            
            Particle muon4 = new Particle();
            muon4.px = (Double.parseDouble(scanner.next()))/1000;
            muon4.py = (Double.parseDouble(scanner.next()))/1000;
            muon4.pz = (Double.parseDouble(scanner.next()))/1000;
            muon4.Q = Double.parseDouble(scanner.next());
            double muon4_energy = muon4.Energy(mass);
            
            //Print the values of the muon momenta
            System.out.println("Muon 1 px = " + muon1.px);
            System.out.println("Muon 1 py = " + muon1.py);
            System.out.println("Muon 1 pz = " + muon1.pz);
            System.out.println("Muon 1 Q = " + muon1.Q);
            System.out.println("Muon 1 E = " + muon1_energy);
            
            System.out.println("Muon 2 px = " + muon2.px);
            System.out.println("Muon 2 py = " + muon2.py);
            System.out.println("Muon 2 pz = " + muon2.pz);
            System.out.println("Muon 2 Q = " + muon2.Q);
            System.out.println("Muon 2 E = " + muon2_energy);
            
            System.out.println("Muon 3 px = " + muon3.px);
            System.out.println("Muon 3 py = " + muon3.py);
            System.out.println("Muon 3 pz = " + muon3.pz);
            System.out.println("Muon 3 Q = " + muon3.Q);
            System.out.println("Muon 3 E = " + muon3_energy);
            
            System.out.println("Muon 4 px = " + muon4.px);
            System.out.println("Muon 4 py = " + muon4.py);
            System.out.println("Muon 4 pz = " + muon4.pz);
            System.out.println("Muon 4 Q = " + muon4.Q);
            System.out.println("Muon 4 E = " + muon4_energy);
            
            //add the fourvectors of the 4 muons
            Particle combinedParticle = new Particle();
            combinedParticle.px = muon1.px + muon2.px + muon3.px + muon4.px;
            combinedParticle.py = muon1.py + muon2.py + muon3.py + muon4.py;
            combinedParticle.pz = muon1.pz + muon2.pz + muon3.pz + muon4.pz;
            combinedParticle.E = muon1_energy + muon2_energy + muon3_energy + muon4_energy;
            
            //compute combined mass
            double combined_mass = combinedParticle.mass();
            
            //print combined mass
            System.out.println("Combined mass = " + combined_mass);
            
            //fill histogram
            //mass4mu.fill(combined_mass);
            
            //***********************************************************************************************
            
            //Create 4 muons which have errors in their transverse momentum, distributed following a gaussian
            double pT1 = Math.sqrt((muon1.px*muon1.px)+(muon1.py*muon1.py));
            double theta_detector1 = Math.sqrt(((r0/pT1)*(r0/pT1))+(r1*r1)+((r2*pT1)*(r2*pT1)));
            Particle muon1_measured = new Particle();
            double error1 = randGen.nextGaussian()*theta_detector1*pT1; 
            muon1_measured.px = (muon1.px)*(error1+1);
            muon1_measured.py = (muon1.py)*(error1+1);
            muon1_measured.pz = (muon1.pz)*(error1+1);
            muon1_measured.Q = muon1.Q;
            double muon1_energy_measured = muon1_measured.Energy(mass);
            
            double pT2 = Math.sqrt((muon2.px*muon2.px)+(muon2.py*muon2.py));
            double theta_detector2 = Math.sqrt(((r0/pT2)*(r0/pT2))+(r1*r1)+((r2*pT2)*(r2*pT2)));
            Particle muon2_measured = new Particle();
            double error2 = randGen.nextGaussian()*theta_detector2*Math.sqrt(muon2.px*muon2.px + muon2.py*muon2.py); 
            muon2_measured.px = (muon2.px)*(error2+1);
            muon2_measured.py = (muon2.py)*(error2+1);
            muon2_measured.pz = (muon2.pz)*(error2+1);
            muon2_measured.Q = muon2.Q;
            double muon2_energy_measured = muon2_measured.Energy(mass);
            
            double pT3 = Math.sqrt((muon3.px*muon3.px)+(muon3.py*muon3.py));
            double theta_detector3 = Math.sqrt(((r0/pT3)*(r0/pT3))+(r1*r1)+((r2*pT3)*(r2*pT3)));
            Particle muon3_measured = new Particle();
            double error3 = randGen.nextGaussian()*theta_detector3*Math.sqrt(muon3.px*muon3.px + muon3.py*muon3.py); 
            muon3_measured.px = (muon3.px)*(error3+1);
            muon3_measured.py = (muon3.py)*(error3+1);
            muon3_measured.pz = (muon3.pz)*(error3+1);
            muon3_measured.Q = muon3.Q;
            double muon3_energy_measured = muon3_measured.Energy(mass);
            
            double pT4 = Math.sqrt((muon4.px*muon4.px)+(muon4.py*muon4.py));
            double theta_detector4 = Math.sqrt(((r0/pT4)*(r0/pT4))+(r1*r1)+((r2*pT4)*(r2*pT4)));
            Particle muon4_measured = new Particle();
            double error4 = randGen.nextGaussian()*theta_detector4*Math.sqrt(muon4.px*muon4.px + muon4.py*muon4.py); 
            muon4_measured.px = (muon4.px)*(error4+1);
            muon4_measured.py = (muon4.py)*(error4+1);
            muon4_measured.pz = (muon4.pz)*(error4+1);
            muon4_measured.Q = muon4.Q;
            double muon4_energy_measured = muon4_measured.Energy(mass);
            
             //Print the values of the muon momenta
            System.out.println("Muon 1 measured px = " + muon1_measured.px);
            System.out.println("Muon 1 measured py = " + muon1_measured.py);
            System.out.println("Muon 1 measured pz = " + muon1_measured.pz);
            System.out.println("Muon 1 measured Q = " + muon1_measured.Q);
            System.out.println("Muon 1 E = " + muon1_energy);
            
            System.out.println("Muon 2 measured px = " + muon2_measured.px);
            System.out.println("Muon 2 measured py = " + muon2_measured.py);
            System.out.println("Muon 2 measured pz = " + muon2_measured.pz);
            System.out.println("Muon 2 measured Q = " + muon2_measured.Q);
            System.out.println("Muon 2 measured E = " + muon2_energy_measured);
            
            System.out.println("Muon 3 measured px = " + muon3_measured.px);
            System.out.println("Muon 3 measured py = " + muon3_measured.py);
            System.out.println("Muon 3 measured pz = " + muon3_measured.pz);
            System.out.println("Muon 3 measured Q = " + muon3_measured.Q);
            System.out.println("Muon 3 measured E = " + muon3_energy_measured);
            
            System.out.println("Muon 4 measured px = " + muon4_measured.px);
            System.out.println("Muon 4 measured py = " + muon4_measured.py);
            System.out.println("Muon 4 measured pz = " + muon4_measured.pz);
            System.out.println("Muon 4 measured Q = " + muon4_measured.Q);
            System.out.println("Muon 4 measured E = " + muon4_energy_measured);
            
            //add the fourvectors of the 4 muons
            Particle combinedParticleMeasured = new Particle();
            combinedParticleMeasured.px = muon1_measured.px + muon2_measured.px + muon3_measured.px + muon4_measured.px;
            combinedParticleMeasured.py = muon1_measured.py + muon2_measured.py + muon3_measured.py + muon4_measured.py;
            combinedParticleMeasured.pz = muon1_measured.pz + muon2_measured.pz + muon3_measured.pz + muon4_measured.pz;
            combinedParticleMeasured.E = muon1_energy_measured + muon2_energy_measured + muon3_energy_measured + muon4_energy_measured;
            
            //compute combined mass
            double combined_mass_measured = combinedParticleMeasured.mass();
            
            //print combined mass measured
            System.out.println("Combined mass measured = " + combined_mass_measured);
            
            //print combined mass
            System.out.println("Combined mass = " + combined_mass);
            
            //fill histogram
            mass4muMeasured.fill(combined_mass_measured);
                    
        }
         
        //close the scanner  
        scanner.close();
        
        //write contents of histograms to csv file and print
        mass4mu.print();
        mass4mu.writeToDisk("m(4mu) histogram " + lines + " events.csv");
        
        mass4muMeasured.print();
        mass4muMeasured.writeToDisk("m(4mu) measured histogram " + lines + " events.csv");
        
    }
    
}
