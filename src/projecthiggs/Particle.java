package projecthiggs;

public class Particle {
 // store the components of the FourVector
    
    double E;
    double mass;
    double Q;
    double px;
    double py;
    double pz;

    double momentum()
    {
        double p = px*px+py*py+pz*pz;
        p = Math.sqrt(p);
        return p;
    }
    
    double mass()
    {
        double m = Math.sqrt((E*E)-(px*px)-(py*py)-(pz*pz));
        return m;
    }       
    
    double Lorentz()
    {
        double Y = E/mass();
        return Y;
    }            

    double Beta()
    {
        double beta = momentum()/E;
        return beta;
    }
    
    double CurvatureRadius(double B)
    {
        double r = momentum()/(300*B);
        return r;
    }

    double DistanceTravelled(double t_particle)
    {        
        double t_observer = Lorentz()*t_particle;
        double d = t_observer*(Beta()*3E8);
        return d;
    }
    
    double Energy(double mass)
    {
        double muonE = Math.sqrt((mass*mass)+(px*px)+(py*py)+(pz*pz));
        return muonE;
    }
}

